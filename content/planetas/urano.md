---
title: 'Urano'
layout: "single"
image: "/images/urano.png"
caracteristicas: [
    "Distância Média do Sol: 2,8 bilhões de km",
    "Diâmetro: 51.188 km",
    "Temperatura média da superfície: -190°C",
    "Rotação: 17 horas e 14 minutos",
    "Translação: 84 anos",
    "Satélites: 27 conhecidos",
]
---

## Descrição

O terceiro planeta gasoso do sistema solar - depois de Júpiter e Saturno - não era conhecido até meados do século XVIII. Foi descoberto em 1781 pelo astrônomo William Herschel. A identificação de seu sistema de anéis é ainda mais recente - ocorreu em 1977. Uma de suas características mais marcantes é a inclinação do eixo de rotação que, durante grande parte do movimento de translação, "esconde" completamente do Sol um de seus pólos.

## Atmosfera

A atmosfera de Urano alcança milhares de quilômetros de altitude. Apresenta menor taxa de hidrogênio, se comparado a Júpiter e Saturno - cerca de 83%. é constituído também de hélio, uma pequena quantidade de metano e outros compostos. A combinação química confere ao planeta uma cor azul-esverdeada.