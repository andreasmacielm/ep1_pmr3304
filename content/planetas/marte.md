---
title: 'Marte'
layout: "single"
image: "/images/marte.png"
caracteristicas: [
    "Distância Média do Sol: 228 milhões de km",
    "Diâmetro: 6.794 km",
    "Variação da temperatura na superfície: -140°C q 22°C",
    "Rotação: 24 horas e 37 minutos",
    "Translação: 687 dias",
    "Satélites: 2"
]
---

## Descrição

A cor veremlha de Marte sempre foi associada a forças destruidoras, por isso o planeta foi batizado com o nome do deus romano da guerra - para os gregos, Ares. Seus dois satélites naturais, receberam os nome dos filhos de Ares com Afrodite: Fobos e Deimos. O planeta sempre despertou a curiosidade humana. Por algum tempo, imaginou-se que o misterioso mundo era habitado por seres inteligentes, até que imagens mostraram a desolada paisafem marciana. A busca atual é por indícios de vida microscópica.

## Missões não Tripuladas 

As primeiras sondas a se aproximarem do planeta vermelho, no final da década de 1960, foram as norte americanas Mariner, 4,6 e 7. Em 1971, uma cápsula de sonda soviética Marte 3 pousou no planeta. De lá para cá, outras missões capturaram preciosas informações sobre relevo, atmosfera, composição e estrutura de Marte.

Constatou-se um relevo bastante diversificado, com imensos vulcões, cânions gigantes, planícies, dunas, crateras, calotas polares e vales. Pode ter sido esculpido por água corrente que teria existidoem um passado muito distante. Há, no entanto,pesquisadores que acreditam que o provável agente modificador do relevo tenha sido a lava vulcânica.
Supõem-se no entanto que exista água congelada no planeta, na regiâo próximas dos pólos e sob sua superfície. 
