---
title: 'Vênus'
layout: "single"
image: "/images/venus.png"
caracteristicas: [
    "Distância Média do Sol: 108,2 milhões de km",
    "Diâmetro: 12.104 km",
    "Variação de temperatura na superfície: 75°C a 425°C",
    "Rotação: 243 dias",
    "Translação: 224,7 dias",
    "Satélites: não tem",
]

---

## Descrição

Segundo corpo celeste que mais brilha no crepúsculo depois da Lua, Vênus é similar à Terra em muitos aspectos. Ambos têm dimensão, massa e tdensidade aproximadas. As sondas espaciais que lá chegaram, porém, mostraram que Vênus apresenta condições bem diferentes em relação às da Terra. Em Vênus não existe água, sua atmosfera é muiro mais densa e carregada de gás carbônico e ali ocorre um efeito estufa permanente - a temperatura em sua superfície é a mais alta dentre os planetas. 

Vênus é conhecida como Esrela d'Álva porque é visível no céu antes do nascer do Sol. Seu nome é uma homenagem à deusa romana do amor.

## Relevo

Recentemente, em 1990, a sonda Mallegan mapeou a maior parte de sua superfície. O relevo de Vênus é formado por amplas planícies onduladas e por extensos planaltos. Um deles é chamado de terra Aphrodite - um gigantesco maciço montanhoso que se estende por quase 10 mil quilômetros na região equatorial. Já os Montes Maxwell chegam a ter mais de 10 km de altura. Há ainda crateras resultantes de impactos espaciais, fossas e fraturas. Pelo menos 85% da superfície é coberta de rocha de origem vulcânica mas, apesar disso, não existem registros de atividade vulcãnica no planeta.