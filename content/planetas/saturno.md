---
title: 'Saturno'
layout: "single"
image: "/images/saturno.png"
caracteristicas: [
    "Distância Média do Sol: 1,4 bilhões de km",
    "Diâmetro: 120.536 km",
    "Temperatura média da superfície: -180°C",
    "Rotação: 10 horas e 39 minutos",
    "Translação: 29,4 anos",
    "Satélites: 34 conhecidos",
]
---

## Descrição

Na mitologia grega seu nome corresponde a Cronos, o deus do tempo. Os povos antigos já haviam percebido que a trajetória desse mundo era mais lenta do que a dos outros quatro planetas conhecidos, além da Terra. Só não sabiam que isso ocorria pelo fato de estar mais afastado do Sol. Saturno é o segundo maior planeta do sistema solar e caracteriza-se por seu complexo sistema de anéis, formado por rochas, partículas de gelo e poeira cósmica.

## Anéis

A órbita de Saturno possui milhares de anéis que, vistos à distância, apresentam faixas multicoloridas. Embora pareçam contínuos quando observados da Terra, eles são compostos de rochas, gelo, poeira cósmica e outras partículas e têm tamanhos que cariam de poucos milímetros a alguns quilômetros. Calcula-se que a largura do sistema de anéis seja de 250 mil quilômetros, bastante desproporcional em relação à sua espessura, que não ultrapassa um quilômetro.