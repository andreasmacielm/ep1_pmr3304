---
title: 'Netuno'
layout: "single"
image: "/images/netuno.png"
caracteristicas: [
    "Distância Média do Sol: 4,5 bilhões de km",
    "Diâmetro: 49.248 km",
    "Temperatura média da superfície: -220°C",
    "Rotação: 16 horas e 11 minutos",
    "Translação: 164,7 anos",
    "Satélites: 13 conhecidos",
]
---

## Descrição

A descoberta de Netuno representou um marco para a astronomia. Por causa da órbita irregular de Urano, os estudiosos especularam sobre a existência de um oitavo planeta, que seria o responsável pelos desvios observados naquela rota. Em 1846, o inglês John Couch Adams e o francês Urbain Le Verrier calcularam o local onde ele estaria. Le Verrier apresentou suas conjecturas ao alemâo Johann Gottfried Galle que localizou Netuno, planeta cujo nome remete ao deus romano dos oceanos.

## Diversas luas

Até 1989 , quando a Voyager 2 iniciou sua exploração do planeta, só eram cinhecidos dois satélites de Netuno: Nereida e Tritão. Este último, identificado um mês após a localização do planeta, é uma das maiores luas do sistema solar, e a única que orbita um planeta em movimento retrógrado. No total, Netuno tem 13 luas conhecidas, das quais seis foram descobertas pela Voyager 2. Uma destas, Proteu, é pouco maior que Nereida, e a única cujo período de translação é maior do que a rotação do planeta.
