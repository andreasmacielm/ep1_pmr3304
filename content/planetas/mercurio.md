---
title: 'Mercúrio'
layout: "single"
image: "/images/mercurio.png"
caracteristicas: [
    "Distância Média do Sol: 57,9 milhões de km",
    "Diâmetro: 4.878 km",
    "Variação de temperatura na superfície: -150°C a 350C",
    "Rotação: 59 dias",
    "Translação: 87,9 dias",
    "Satélites: não tem",
]
---

## Descrição

Uma das particularidades de Mercúrio, o segundo menor planeta do sistema solar e o mais próximo do Sol, é sua difícil visualização. Os antigos pensavam tratar-se de dois planetas, porque Mercúrio era observado ora em uma posição no céu, ora em outra. De sua superfície, o Sol seria quase três vezes maior do que quando visto na Terra. Por ser o planeta de translação mais veloz - seu ano dura 88 dias - os gregos chamaram-no de Hermes, o mensageiro do Olimpo. Mercúrio, para os romanos é o correspondente do deus Hermes.

## Missão Mariner

A maior parte dos dados conhecidos de Mercúrio foi obtida pela sonda espacial Mariner 10, lançada pelos Estados Unidosem 1973 e que visitou o planeta entre 1974 e 1975. Evidenciando imagens com um relevo semelhante ao da Lua, com depressões, vales e crateras, provavelmente choques de meteoritos que atravessavam sua incipiente atmosfera e colidiam contra a superfície.