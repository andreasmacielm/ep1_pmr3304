---
title: 'Terra'
layout: "single"
image: "/images/terra.png"
caracteristicas: [
    "Distância Média do Sol: 149,6 milhões de km",
    "Diâmetro: 12.756 km",
    "Variação de temeperatura na superfície: de -50°C a 58°C",
    "Rotação: 1 dia",
    "Translação: 365 dias",
    "Satélites: 1 a Lua",
]
---

## Descrição

O terceiro planeta mais próximo do Sol, um pequeno mundo dentro da vastidão do cosmo. Pequeno, mas precioso - afinal, é a nossa casa e, pelo que se sabe até hoje, o único lugar a abrigar formas de vida. Dentre os planetas rochosos é o maior, conhecido também como planeta azul por ter cerca de 70% da superfície coberta por água, sendo o único planeta a apresentar esta substância nos três estados (sólido, líquido e gasoso), fator considerado essencial para o desenvolvimento da vida.

## Formação

Originou-se a partir de gás e poeira cósmica, em torno das quais foram agregando-se, com o tempo, materiais sólidos vindos de choques e fragmentos de rocha cada vez maiores.

Uma combinação de elementos químicos teria gerado um intenso calor, fazendo da Terra uma enorme massa esférica pastosa. Os materiais mais densos, ricos em ferro e níquel, foram empurrados em direção ao núcleo pela força da gravidade. Os menos densos chegaram à superfície e cristalizaram-se, formando a primeira crosta terrestre, acima de outra camada mais densa, chamada de manto.

Durante o processo de resfriamento, gases e vapores eram jogados para a atmosfera ao mesmo tempo que novos componentes chegavam junto do bombaardemanto de cometas e asteróides. A abundância de vapor d'água, em razão dos sucessivos choques com os corpos, provocou sua condensação, e chuvas ininterruptas formaram os lagos, lagoas, mares e um oceano, que envolvia a terra firme.
