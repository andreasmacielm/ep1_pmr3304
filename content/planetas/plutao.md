---
title: 'Plutão'
layout: "single"
image: "/images/plutao.png"
caracteristicas: [
    "Distância Média do Sol: 5,9 bilhões de km",
    "Diâmetro: 2.320 km",
    "Temperatura média da superfície: -230°C",
    "Rotação: 6,3 dias",
    "Translação: 248,4 anos",
    "Satélites: 1",
]
---

## Descrição

Descoberto em 1930, Plutão é o menor dos planetas do sistema solar e o menos conhecido. Esta tão afastado que alguns astrônomos sugerem que deveria ser classificado como um grande asteróide ou cometa do grupo de corpos celestes do chamado Cinturão de Kuiper, localizado alpem da órbita de Netuno. A enorme distância da Terra e a dificuldade de observá-lo fizeram com que recebesse o nome do deus romano do inferno e das profundesas terrestres.

## Satélite Caronte

Plutão tem um satélite natural, Caronte - que, na mitologia grega, é o barqueiro que conduz as almas dos mortos pelos rios do inferno. Descoberto em 1978, Caronte possui 1.172 km de diâmetro e caracteriza-se por ser a maior lua em relação ao tamanho do planeta. Por esse motivo, há astrônomos que dizem que Plutão e Caronte são um planeta duplo, e não planeta e e lua. Supõem-se que a composição de Caronte seja a mesma de Plutão. Outra curiosidade a respeito do satélite é que o período de sua órbita em torno do planeta é exatamente o mesmo que o de sua rotação. Assim, Caronte tem sempre a mesma face virada para Plutão.
