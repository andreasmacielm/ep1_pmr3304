---
title: 'Júpiter'
layout: "single"
image: "/images/jupiter.png"
caracteristicas: [
    "Distância Média do Sol: 778 milhões de km",
    "Diâmetro: 143.000 km",
    "Temperatura média da superfície: -147°C",
    "Rotação: 9 horas e 50 minutos",
    "Translação: 11,9 anos",
    "Satélites: 63 conhecidos",
]
---

## Descrição

O primeiro planeta dos chamados gigantes gasosos teve seu nome inspirado no deus grego Zeus, o rei do Olimpo. A composição de Júpiter - hélio e hidrogênio - é muito similar à de estrelas como o Sol. O maior planeta do sistema solar possui um campo magnético muito intenso e sua pressão é de 3 milhões de vezes mais forte do que a da Terra. Chama atenção pela Grande Mancha Vermelha - uma região de tempestades - e por ter o dia mais curto, com rotação de 9 horas e 50 minutos.

## Satélites

Em 1610, o astrônomo Galileu Galilei foi o primeiro a observar, com seu famoso telescópio, quatro satélites naturais de Júpiter. Eram os que mais brilhavam: Io, Europa, Ganimedes e Calisto. Outras luas - Amaltéia, Himalia, Elara, Pasifae, Sínope, Lisitéia, Carme, Ananque, Leda, Tebe, Métis, e Adrastéia - foram descobertas mais tarde, principalmente entre 1892 e 1979.

