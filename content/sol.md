---
title: 'Sol'
layout: "single"
image: "/images/sol.png"
caracteristicas: [
    "Composição: 90% Hidrogênio (H); 9,9% Hélio (He); e o restante divide entre Carbono (C), Nitrogênio (N) e Silício (Si), entre outros",
    "Diâmetro: 1.392.000 km",
    "Temperatura média da superfície: 5700°C",
    "Idade: cerca de 4,6 bilhões de anos"

]
---

## Descrição

O sol é uma gigantesca esfera composta quase que inteiramente de gases. Concenetra 99% da massa do sistema solar. Localiza-se a cerca de 150 milhões de quilômetros da Terra - a luz emitida pela estrela leva cerca de 8 minutos para chegar a superfície terrestre. A energia gerada pelo sol é a responsável pela existência da vida.

## Meia-vida

O núcleo solar é onde ocorrem as reações nucleares (fusão do hidrogênio se transformando em hélio), sendo que a energia liberada se propaga por sucessivas camadas até chegar à superfície. Calcula-se que a temperatura da superfície do Sol seja de aproximadamente 5.700°C, enqaunto em seu interior pode chegar a vários milhões de graus.

A luz que emite viaja a 300 mil km/s - a velocidade da luz - e leva 8 minutos e 30 segundos para chegar à Terra. Estima-se que o brilho aumente 10% a cada um bilhão de anos. Pela previsão dos cientistas, é provável que o Sol esteja na metade de sua vida e que ainda brilhará por mais 5 bilhões de anos antes de entrar em colapso e apagar-se.