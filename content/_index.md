---
title: 'Sistema Solar'

image: "/images/sistemasolar2.jpg"
---
## Introdução

Hoje Sabemos que o sistema solar, localizado em um braço da Via Láctea, distante cerca de 32 mil anos-luz do centro da galáxia, é o nosso "endereço" cósmico. Porém, muito tempo se passou até que fosse descoberto o verdadeiro lugar da Terra no cosmo. Afinal, durante séculos imaginou-se que o nosso planeta fosse o centro do universo, com os demais astros movendo-se ao seu redor. Apenas no século XVI foi possível demonstrar que a Terra é que girava em torno do Sol. Este sim ocupava a posição central, tendo os planetas em sua órbita.

## Origem

O sitema solar é constituído pelo sol e por uma série de corpos celestes que gravitam em torno dele. Fazem parte desse sistema os nove planetas conhecidos - na ordem a partir do mais próximo do sol: Mercúrio, Vênus, Terra, Marte, Júpiter, Saturno, Urano, Netuno e Plutão - e seus satélites naturais, cometas e asteróides, além de poeira e gases cósmicos.

A teoria mais aceita sobre sua origem sustenta que ele foi formado há cerca de 5 bilhões de anos, a partir de uma imensa nuvem de poeira e gás - especialmente hidrogênio e hélio. Forças gravitacionas teriam concentrado a maior parte da massa de nuvem em um único ponto, onde blocos de rochas e gases começaram a agregar-se. Esse ponto ficou cada vez mais denso e concentrado e sua força gravitacional reteve os gases, o que desencadeou uma série de reações químicas e fisicas que elevaram muito a temperatura dando, assim, origem a uma estrela com fonte de energia própria: o Sol. Esse processo extremamente lento consumiu centenas de milhões de anos.

Os blocos, chamados de planetesimais, começaram a chocar-se continuamente, originando esferas cada vez maiores. Com o decorrer do tempo e sob ação da força gravitacional, eles deram origem aos atuais planetas.